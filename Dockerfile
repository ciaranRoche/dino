FROM golang:1.15
ENV DATABASE_HOST=xxxx
ENV DATABASE_PASSWORD=xxxx
WORKDIR $GOPATH/src/github.com/ciaranRoche/dino
COPY . .
RUN go get -d -v ./...
RUN go install -v ./...
EXPOSE 8080
CMD ["dino"]