module github.com/ciaranRoche/dino

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	github.com/rs/cors v1.7.0 // indirect
	github.com/segmentio/ksuid v1.0.3
	github.com/sirupsen/logrus v1.7.0
	gopkg.in/gormigrate.v1 v1.6.0
)
