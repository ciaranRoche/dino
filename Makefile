IMAGE_REG=quay.io
IMAGE_ORG=croche
IMAGE_NAME=dino
DATABASE_HOST=localhost
DATABASE_PASSWORD=postgres


SHELL=/bin/bash

.PHONY: image/build
image/build:
	docker build -t $(IMAGE_REG)/$(IMAGE_ORG)/$(IMAGE_NAME) .

.PHONY: image/push
image/push: image/build
	docker push $(IMAGE_REG)/$(IMAGE_ORG)/$(IMAGE_NAME)

.PHONY: run
run:
	DATABASE_HOST=$(DATABASE_HOST) DATABASE_PASSWORD=$(DATABASE_PASSWORD) go run main.go

