package main

import (
	"github.com/ciaranRoche/dino/pkg/db"
	"github.com/ciaranRoche/dino/pkg/logger"
	"github.com/ciaranRoche/dino/pkg/router"
	"net/http"

	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var PORT = ":8080"

func main() {
	r := router.Router()

	db.Migrate()

	log := logger.NewActionLogger("main")
	log.Infof("starting server on %s", PORT)

	log.Fatal(http.ListenAndServe(PORT, r))
}


