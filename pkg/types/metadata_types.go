package types

import "time"

// CollectionMetadata represents a collection.
type CollectionMetadata struct {
	ID   string `json:"id"`
	HREF string `json:"href"`
	Kind string `json:"kind"`
}

// VersionMetadata represents a version.
type VersionMetadata struct {
	ID          string               `json:"id"`
	HREF        string               `json:"href"`
	Kind        string               `json:"kind"`
	Collections []CollectionMetadata `json:"collections"`
}

// Metadata api metadata.
type Metadata struct {
	ID       string            `json:"id"`
	HREF     string            `json:"href"`
	Kind     string            `json:"kind"`
	Versions []VersionMetadata `json:"versions"`
}

// Meta is base model definition, embedded in all kinds
type Meta struct {
	ID        string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}

// List Paging metadata
type PagingMeta struct {
	Page  int
	Size  int
	Total int
}