package types

type Monitor struct {
	CPU int
	GoRouting int
	Memory uint64
}
