package types

import (
	"github.com/ciaranRoche/dino/pkg/api"
	"github.com/jinzhu/gorm"
)

type Dinosaur struct {
	Meta
	Species string
}

type DinosaurList []*Dinosaur
type DinosaurIndex map[string]*Dinosaur


func (org *Dinosaur) BeforeCreate(scope *gorm.Scope) error {
	return scope.SetColumn("ID", api.NewID())
}

type DinosaurPatchRequest struct {
	Species *string `json:"species,omitempty"`
}
