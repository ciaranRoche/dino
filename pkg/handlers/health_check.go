package handlers

import (
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
)

type HealthCheckHandler struct {
	log            *logrus.Entry
}

func NewHealthCheckHandler(logger *logrus.Entry) *HealthCheckHandler {
	return &HealthCheckHandler{
		log: logger.WithFields(logrus.Fields{"handler": "healthCheck"}),
	}
}

func (h HealthCheckHandler) HealthCheck(w http.ResponseWriter, r *http.Request) {
	h.log.Info("health check called")
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	io.WriteString(w, `{"alive": true}`)
}