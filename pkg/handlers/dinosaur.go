package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/ciaranRoche/dino/pkg/api"
	db2 "github.com/ciaranRoche/dino/pkg/db"
	"github.com/ciaranRoche/dino/pkg/types"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"net/http"
)

var _ RestHandler = dinosaurHandler{}

type dinosaurHandler struct{
	log            *logrus.Entry
}

func NewDinosaurHandler(logger *logrus.Entry) *dinosaurHandler{
	return &dinosaurHandler{
		log: logger.WithFields(logrus.Fields{"handler": "dinosaur"}),
	}
}

func (d dinosaurHandler) List(w http.ResponseWriter, r *http.Request) {
	d.log.Infof("dinosaur list called")
	db, err := db2.DBConnection()
	if err != nil {
		d.log.Errorf("failed to connect to DB : %s", err)
		fmt.Fprintf(w, "failed to connect to DB")
	}
	defer db.Close()

	var dinos []types.Dinosaur
	db.Find(&dinos)
	json.NewEncoder(w).Encode(&dinos)
}

func (d dinosaurHandler) Get(w http.ResponseWriter, r *http.Request) {
	d.log.Infof("dinosaur get called")

	id := mux.Vars(r)["id"]

	db, err := db2.DBConnection()
	if err != nil {
		d.log.Errorf("failed to connect to DB : %s", err)
		fmt.Fprintf(w, "failed to connect to DB")
	}
	defer db.Close()

	dino := &types.Dinosaur{}

	db.Where("species = ?", id).First(&dino)

	json.NewEncoder(w).Encode(&dino)
}

func (d dinosaurHandler) Create(w http.ResponseWriter, r *http.Request) {
	d.log.Info("dinosaur create called")
	db, err := db2.DBConnection()
	if err != nil {
		d.log.Errorf("failed to connect to DB : %s", err)
		fmt.Fprintf(w, "failed to connect to DB")
	}
	defer db.Close()

	var apiDino api.Dinosaur
	err = JSONUnmarshal(r.Body, &apiDino)
	if err != nil {
		d.log.Errorf("failed to unmarshall body : %s", err)
		fmt.Fprint(w, "failed to unmarshall body")
	}

	if apiDino.Species != ""{
		db.Create(&types.Dinosaur{
			Species: apiDino.Species,
		})
	}

	d.List(w, r)
}

func (d dinosaurHandler) Patch(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}

func (d dinosaurHandler) Delete(w http.ResponseWriter, r *http.Request) {
	d.log.Infof("dinosaur delete called")

	db, err := db2.DBConnection()
	if err != nil {
		d.log.Errorf("failed to connect to DB : %s", err)
		fmt.Fprintf(w, "failed to connect to DB")
	}
	defer db.Close()

	id := mux.Vars(r)["id"]

	db.Where("species = ?", id).Delete(&types.Dinosaur{})

	d.List(w, r)
}
