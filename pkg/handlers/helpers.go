package handlers

import (
	"encoding/json"
	"io"
)

func JSONUnmarshal(r io.Reader, i interface{}) error {
	dec := json.NewDecoder(r)
	return dec.Decode(i)
}
