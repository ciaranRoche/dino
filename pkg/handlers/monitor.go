package handlers

import (
	"encoding/json"
	"github.com/ciaranRoche/dino/pkg/service"
	"github.com/sirupsen/logrus"
	"net/http"
)

var _ RestHandler = monitorHandler{}

type monitorHandler struct {
	monitorService *service.MonitorService
	log *logrus.Entry
}

func NewMonitorHandler(logger *logrus.Entry) *monitorHandler {
	return &monitorHandler{
		monitorService: service.NewMonitorService(logger),
		log: logger.WithFields(logrus.Fields{"handler": "monitor"}),
	}
}

func (m monitorHandler) List(w http.ResponseWriter, r *http.Request) {
	monitorStats := m.monitorService.List()
	json.NewEncoder(w).Encode(monitorStats)

}

func (m monitorHandler) Get(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}

func (m monitorHandler) Create(w http.ResponseWriter, r *http.Request) {
	m.monitorService.Calculate()
}

func (m monitorHandler) Patch(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}

func (m monitorHandler) Delete(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}
