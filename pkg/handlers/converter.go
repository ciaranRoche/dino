package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/ciaranRoche/dino/pkg/wordprocessing"
	"github.com/sirupsen/logrus"
	"log"
	"net/http"
)

type UnConvertedWords struct {
	Words string `json:"words,omitempty"`
}

type ConvertedWords struct {
	ConvertedWords string
}

var _ RestHandler = converterHandler{}

type converterHandler struct{
	log            *logrus.Entry
}

func NewConverterHandler(logger *logrus.Entry) *converterHandler{
	return &converterHandler{
		log: logger.WithFields(logrus.Fields{"handler": "convert"}),
	}
}

func (c converterHandler) List(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}

func (c converterHandler) Get(w http.ResponseWriter, r *http.Request) {
	c.log.Infof("convert list called")
	sentence := "to convert your own sentence send a post to /convert"
	processorPipeline := wordprocessing.NewWordProcessingPipeline()

	preProcessingAction, err := wordprocessing.NewPreProcessingAction()
	if err != nil {
		log.Fatal(err)
	}

	spongeBobAction, err := wordprocessing.NewSpongeBobAction()
	if err != nil {
		log.Fatal(err)
	}

	processorPipeline.AddAction(preProcessingAction)
	processorPipeline.AddAction(spongeBobAction)

	processedWord, err := processorPipeline.Transform(sentence)
	if err != nil {
		log.Fatal(err)
	}

	json.NewEncoder(w).Encode(&ConvertedWords{
		ConvertedWords: processedWord,
	})
}

func (c converterHandler) Create(w http.ResponseWriter, r *http.Request) {
	c.log.Info("convert list called")
	var sentence UnConvertedWords
	err := JSONUnmarshal(r.Body, &sentence)
	if err != nil {
		fmt.Fprintf(w, "failed to unmarshal")
	}
	if sentence.Words == "" {
		c.log.Info("words can not be nil defaulting")
		sentence.Words = "words can not be nil"
	} else {
		c.log.Infof("converting words %s", sentence.Words)
	}

	processorPipeline := wordprocessing.NewWordProcessingPipeline()

	preProcessingAction, err := wordprocessing.NewPreProcessingAction()
	if err != nil {
		log.Fatal(err)
	}

	spongeBobAction, err := wordprocessing.NewSpongeBobAction()
	if err != nil {
		log.Fatal(err)
	}

	processorPipeline.AddAction(preProcessingAction)
	processorPipeline.AddAction(spongeBobAction)

	processedWord, err := processorPipeline.Transform(sentence.Words)
	if err != nil {
		log.Fatal(err)
	}

	json.NewEncoder(w).Encode(&ConvertedWords{
		ConvertedWords: processedWord,
	})
}

func (c converterHandler) Patch(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}

func (c converterHandler) Delete(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}
