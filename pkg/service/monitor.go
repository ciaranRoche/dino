package service

import (
	"fmt"
	"github.com/ciaranRoche/dino/pkg/api"
	"github.com/sirupsen/logrus"
	"runtime"
)


type MonitorService struct {
	log *logrus.Entry

}

func NewMonitorService(logger *logrus.Entry) *MonitorService {
	return &MonitorService{
		log: logger.WithFields(logrus.Fields{"service": "monitor"}),
	}
}

func (m MonitorService) List() api.Monitor {
	m.log.Info("getting service stats")
	mem := &runtime.MemStats{}

	cpu := runtime.NumCPU()
	m.log.Infof("number of cpu : %d", cpu)
	rot := runtime.NumGoroutine()
	m.log.Infof("number of go routine %d", rot)
	runtime.ReadMemStats(mem)
	m.log.Infof("memory usage %d", mem.Alloc)

	return api.Monitor{
		CPU: cpu,
		GoRouting: rot,
		Memory: mem.Alloc,
	}
}

func (m MonitorService) Calculate() {
	x := 0
	for {
		x++
		fmt.Println(x)
	}
}
