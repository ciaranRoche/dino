package router

import (
	"github.com/ciaranRoche/dino/pkg/handlers"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"net/http"
)

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger := logrus.WithFields(logrus.Fields{"router": "dino logging"})
		logger.Info(r.RequestURI)
		next.ServeHTTP(w, r)
	})
}

func Router() *mux.Router {
	r := mux.NewRouter()

	logger := logrus.WithFields(logrus.Fields{"router": "dino service"})

	dinosaurHandler := handlers.NewDinosaurHandler(logger)
	healthCheckHandler := handlers.NewHealthCheckHandler(logger)

	r.HandleFunc("/health", healthCheckHandler.HealthCheck)

	r.Use(loggingMiddleware)

	r.HandleFunc("/", dinosaurHandler.List).Methods(http.MethodGet)
	r.HandleFunc("/", dinosaurHandler.Create).Methods(http.MethodPost)
	r.HandleFunc("/{id}", dinosaurHandler.Get).Methods(http.MethodGet)
	r.HandleFunc("/{id}", dinosaurHandler.Delete).Methods(http.MethodDelete)

	return r
}
