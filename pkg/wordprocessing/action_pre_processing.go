package wordprocessing

import (
	"strings"
)

type actionPreProcessing struct {}

var _ WordAction = actionPreProcessing{}

func NewPreProcessingAction() (WordAction, error) {
	return &actionPreProcessing{}, nil
}

func (a actionPreProcessing) Transform(word string) (string, error) {
	// convert string to lower
	toLower := strings.ToLower(word)
	return toLower, nil
}
