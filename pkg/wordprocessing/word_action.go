package wordprocessing

type WordAction interface {
	Transform(word string) (string, error)
}
