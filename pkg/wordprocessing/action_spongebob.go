package wordprocessing

import (
	"fmt"
	"log"
	"math/rand"
	"strings"
)

type actionSpongeBob struct {}

var _ WordAction = actionSpongeBob{}

func NewSpongeBobAction() (WordAction, error) {
	return &actionSpongeBob{}, nil
}

func (a actionSpongeBob) Transform(word string) (string, error) {
	var stringBuilder strings.Builder
	stringBuilder.Grow(len(word))
	for _, character := range word[:] {
		randomNumber := rand.Intn(2)
		if randomNumber%2==0 {
			if _, err := fmt.Fprintf(&stringBuilder, "%s", strings.ToUpper(string(character))); err != nil {
				log.Fatalf("error formatting string, %v", err)
			}
		} else {
			if _, err := fmt.Fprintf(&stringBuilder, "%s", string(character)); err != nil {
				log.Fatalf("error formatting string, %v", err)
			}
		}
	}
	return stringBuilder.String(), nil
}