package wordprocessing

import (
	"errors"
	"fmt"
)

type ProcessingPipeline interface {
	AddAction(WordAction)
	Transform(string) (string, error)
}

var _ ProcessingPipeline = &wordProcessingPipeline{}

type wordProcessingPipeline struct {
	wordProcesses []WordAction
}

func NewWordProcessingPipeline() ProcessingPipeline {
	return &wordProcessingPipeline{
		wordProcesses: []WordAction{},
	}
}

func (w *wordProcessingPipeline) AddAction(action WordAction) {
	if action == nil {
		return
	}
	w.wordProcesses = append(w.wordProcesses, action)
}

func (w wordProcessingPipeline) Transform(s string) (string, error) {
	if s == "" {
		return "", errors.New("string can not be nil")
	}
	currentString := s
	for _, process := range w.wordProcesses {
		var err error
		currentString, err = process.Transform(currentString)
		if err != nil {
			return "", fmt.Errorf("failed to transform word : %w", err)
		}
	}
	return currentString, nil
}
