package api

type Monitor struct {
	CPU int `json:"cpu,omitempty"`
	GoRouting int `json:"goRoutine,omitempty"`
	Memory uint64 `json:"memory,omitempty"`
}

