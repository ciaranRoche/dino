package api

import "time"

// Dinosaur struct for Dinosaur
type Dinosaur struct {
	Id        string    `json:"id,omitempty"`
	Kind      string    `json:"kind,omitempty"`
	Href      string    `json:"href,omitempty"`
	Species   string    `json:"species,omitempty"`
	CreatedAt time.Time `json:"created_at,omitempty"`
	UpdatedAt time.Time `json:"updated_at,omitempty"`
}

// ObjectReference struct for ObjectReference
type ObjectReference struct {
	Id   string `json:"id,omitempty"`
	Kind string `json:"kind,omitempty"`
	Href string `json:"href,omitempty"`
}

