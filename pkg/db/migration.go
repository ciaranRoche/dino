package db

import (
	"github.com/ciaranRoche/dino/pkg/logger"
	"time"
)

type Dinosaur struct {
	Model
	Species string `gorm:"index"`
}


// Model represents the base model struct. All entities will have this struct embedded.
type Model struct {
	ID        string `gorm:"primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
}


func Migrate() {
	log := logger.NewActionLogger("migration")
	log.Infof("handling db migration")

	db, err := DBConnection()
	if err != nil {
		log.Errorf("error creating connection to db : %s", err)
	}
	defer db.Close()

	db.AutoMigrate(&Dinosaur{})
}
