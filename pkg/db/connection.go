package db

import (
	"fmt"
	"github.com/ciaranRoche/dino/pkg/logger"
	"github.com/jinzhu/gorm"
	"os"
)

// todo add interface
const (
	port     = 5432
	user     = "postgres"
	dbname   = "postgres"
)
// todo make configurable
var gormConnect = "host=dino-db.claao66q0nsq.eu-west-1.rds.amazonaws.com port=5432 user=postgres dbname=postgres sslmode=disable password=postgres"

func buildConnectionString() string {
	log := logger.NewActionLogger("connectionStringBuilder")
	log.Info("reading environment variables")

	var host, password string
	host = os.Getenv("DATABASE_HOST")
	password = os.Getenv("DATABASE_PASSWORD")

	return fmt.Sprintf("host=%s port=%d user=%s dbname=%s sslmode=disable password=%s", host, port, user, dbname, password)
}

func DBConnection() (db *gorm.DB, err error) {
	log := logger.NewActionLogger("dbConnection")
	log.Infof("creating connection to db")
	return gorm.Open("postgres", buildConnectionString())
}
