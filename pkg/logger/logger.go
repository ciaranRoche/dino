package logger

import (
	"github.com/sirupsen/logrus"
)

const (
	LoggingKeyAction = "action"
)

func NewActionLogger(action string) *logrus.Entry {
	l := logrus.WithFields(logrus.Fields{"server": "dino"})
	return l.WithField(LoggingKeyAction, action)
}
